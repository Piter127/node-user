import { createPool } from 'mysql';

const pool = createPool({
  host: '127.0.0.1',
  port: 3300,
  user: 'root',
  password: 'root',
  database: 'isklep',
});

const errors = {
  PROTOCOL_CONNECTION_LOST: 'Database connection was closed.',
  ER_CON_COUNT_ERROR: 'Database has too many connections.',
  ECONNREFUSED: 'Database connection was refused.',
};

pool.getConnection((err, connection) => {
  if (err && err.code in errors) {
    console.error(errors[err.code]);
  }

  if (connection) {
    connection.release();
  }

  return;
});

export default pool;