import { SECRET } from '../../consts';
import jwt from 'jsonwebtoken';
import { compareSync, hashSync } from 'bcryptjs';
import User, { Credentials, AuthenticatedUser, UserPayload } from './model';

export const authenticate = async ({
  login,
  password,
}: Credentials): Promise<AuthenticatedUser | undefined> => {
  const user = await User.findOne({ login });

  if (user && compareSync(password, user.hash)) {
    const { hash, ...userData } = user.toObject();
    const token = jwt.sign({ userId: user.id }, SECRET);

    return { ...userData, token };
  }
};

export const getAll = async () => await User.find().select('-hash');

export const getById = async (id: any) => await User.findById(id).select('-hash');

export const create = async (payload: UserPayload) => {
  const { login, password } = payload;

  if (await User.findOne({ login })) {
    throw `Username ${login} is already taken`;
  }

  const user = new User(payload);

  if (password) {
    user.hash = hashSync(password);
  }

  await user.save();
};

export const update = async (id: any, payload: UserPayload) => {
  const user = await User.findById(id);

  if (!user) throw 'User not found';

  const { login, password } = payload;

  if (user.login !== login && (await User.findOne({ login }))) {
    throw `Username ${login} is already taken`;
  }

  if (password) {
    user.hash = hashSync(password);
  }

  Object.assign(user, payload);

  await user.save();
};

export const _delete = async (id: any) => {
  await User.findByIdAndRemove(id);
};
