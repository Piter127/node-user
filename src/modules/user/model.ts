import { Schema, Document, model } from 'mongoose';

export type Credentials = {
  login: string;
  password: string;
};

export type SimpleUser = {
  login: string;
  hash: string;
  firstName: string;
  lastName: string;
  createdDate: Date;
}

export type UserPayload = Omit<SimpleUser, 'hash' & 'createdDate'> & {
  password: string;
}

export type AuthenticatedUser = Omit<SimpleUser, 'hash'> & {
  token: string;
};

const schema = new Schema({
  login: { type: String, unique: true, required: true },
  hash: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  createdDate: { type: Date, default: Date.now }, 
});

schema.set('toJSON', { virtuals: true });

export default model<SimpleUser & Document>('User', schema);
