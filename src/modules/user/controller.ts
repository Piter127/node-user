import express, { Router, RequestHandler } from 'express';
import * as userService from './service';

type Request = express.Request & {
  user: {
    userId: string;
  };
}

export const router = Router();

const authenticate: RequestHandler = async (req, res, next) => {
  try {
    const user = await userService.authenticate(req.body);

    user ? res.json(user) : res.status(400).json({ message: 'Login or password is incorrect' });
  } catch (err) {
    next(err);
  }
};

const register: RequestHandler = async (req, res, next) => {
  try {
    await userService.create(req.body);

    res.json({});
  } catch (err) {
    next(err);
  }
};

const getAll: RequestHandler = async (req, res, next) => {
  try {
    const users = await userService.getAll();

    res.json(users);
  } catch (err) {
    next(err);
  }
};

const getCurrent: RequestHandler = async (req: Request, res, next) => {
  try {
    console.log(Object.keys(req));
    const user = await userService.getById(req.user.userId);

    res.json(user);
    res.json({});
  } catch (err) {
    next(err);
  }
};

const getById: RequestHandler = async (req, res, next) => {
  try {
    const user = await userService.getById(req.params.id);

    user ? res.json(user) : res.sendStatus(404);
  } catch (err) {
    next(err);
  }
};

const update: RequestHandler = async (req, res, next) => {
  try {
    await userService.update(req.params.id, req.body);

    res.json({});
  } catch (err) {
    next(err);
  }
};

const _delete: RequestHandler = async (req, res, next) => {
  try {
    await userService._delete(req.params.id);

    res.json({});
  } catch (err) {
    next(err);
  }
};

router.post('/authenticate', authenticate);
router.post('/register', register);

router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);

router.put('/:id', update);

router.delete('/:id', _delete);
