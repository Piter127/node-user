import express from 'express';
import { createServer } from 'http';
import cors from 'cors';
import { connect } from 'mongoose';
import bodyParser from 'body-parser';
import { DB_NAME, MONGO_HOST } from './consts';
import { errorHandler } from './helpers/error-handler';
import { jwt } from './helpers/jwt';
import { router as userRouter } from './modules/user/controller';

(async () => {
  const mongooseConnection = await connect(
    `mongodb://${MONGO_HOST || 'localhost'}:27017/${DB_NAME}`,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  );

  const app = express();
  const server = createServer(app);

  app.set('port', process.env.PORT || 3001);

  app.use(express.static('public'));
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cors());

  app.use(jwt());
  app.use(errorHandler);

  app.use('/user', userRouter);

  // app.get('/test', (req, res) => {
  //   res.send('hello world');
  // });

  server.listen(app.get('port'), () => {
    console.log(`\n🚀 Server is now running on \n http://localhost:${app.get('port')}`);
  });
})();
