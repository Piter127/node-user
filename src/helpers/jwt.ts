import expressJwt, { IsRevokedCallback } from 'express-jwt';
import { SECRET } from '../consts';
import { getById } from '../modules/user/service';

const isRevoked: IsRevokedCallback = async (req, payload, done) => {
  const user = await getById(payload.userId);

  if (!user) {
    return done(null, true);
  }

  return done(null);
};

export const jwt = () =>
  expressJwt({ secret: SECRET, isRevoked }).unless({
    path: ['/user/authenticate', '/user/register'],
  });
