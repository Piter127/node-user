import { ErrorRequestHandler } from 'express';

export const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  // Custom application error
  if (typeof err === 'string') {
    return res.status(400).json({ message: err });
  }

  const { name, message } = err;

  // Mongoose validation error
  if (name === 'ValidationError') {
    return res.status(400).json({ message });
  }

  // JWT authentication error
  if (name === 'UnauthorizedError') {
    return res.status(401).json({ message: 'Invalid token' });
  }

  return res.status(500).json({ message });
};
